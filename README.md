EXAMQUIZ
Create an exam system for your site.

INTRODUCTION
Exam quiz is the easiest way to create an exam on your site.
it can provide a way to create an exam by using two content types (exam, question) and store the result on the user side per exam.
You can provide a score per question and specify if this question is multiple or not.
The exam is successfully passed if the user has a score greater than the min score attached to the exam entity.
You can customize the fail & success messages by getting in on the form setting of this module

For a full description of the module, visit the project page:
https://drupal.org/project/examquiz

To submit bug reports and feature suggestions, or to track changes:
https://drupal.org/project/issues/examquiz



REQUIREMENTS
This module requires no modules outside of Drupal core.


INSTALLATION
Install as you would normally install a contributed Drupal module. Visit:
https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules
for further information.

CONFIGURATION
You can configure the messages in case of failure & success under configuration system.


